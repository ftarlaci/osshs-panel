# **OSSHS**
*Open Source Software Health Score Project*

Please Read the [Health Score Wiki Page](https://gitlab.com/openteams/osshs/-/wikis/Open-Source-Software-Health-Score-Project) for details.




##### **Running the Health Score Panel App** 

We have implemented a panel app to demonstrate the first version of the health score. To run the app locally, please follow the steps below: 

In your shell, create a conda environment from the provided `environment.yml` file with this command:
`conda env create --file environment.yml`
Then, activate the environment you have just created:
`conda activate panel_app`
Finally, start the panel app by running the command:
`panel serve app.py`

This will start a Bokeh localhost server in your browser to query the app for a project name.

This first iteration of the app currently has contributors' data from a sample list of ~500 open source projects with varying health scores.