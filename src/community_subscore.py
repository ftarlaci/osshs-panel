import numpy as np
import pandas as pd




def community_score(df):
    """
    Compute the community subscore with the following rules:
        - Boolean fields (documentation, contribution guidelines
            and readme) counts for 30 points;
            
        - Non boolean fields (open/closed issues and PRs) 
            counts for 70 points 
    """
    project = df.copy()
    bf_score = binary_fields_score(project)
    nbf_score = non_binary_fields_score(project)
    project['community_health_score'] = round(bf_score + nbf_score, 2)

    return project['community_health_score']


def binary_fields_score(project):
    """
    Compute the partial community score (counting for 30 points out of 100)
    based on boolean fields with the following coefficients:
        - documentation          : 0.5
        - conribution guidelines : 0.3
        - readme                 : 0.2
    """
    doc = project['documentation'].apply(lambda x: binarize_field(x))
    contrib = project['has_contributing_guidelines'].apply(lambda x: binarize_field(x))
    readme = project['has_readme'].apply(lambda x: binarize_field(x))
    return 30*(0.5*doc + 0.3*contrib + 0.2*readme)

def non_binary_fields_score(project):
    """
    Compute a weighted score based on non binary fields
    (open/closed issues and PRs)
    """
    issues_prs = ['open_issues_count', 'closed_issues_count', 'open_pr_count', 'closed_pr_count']
    for c in issues_prs:
        project[c] = project[c].apply(lambda x: logscale(clean(x)))
        project['norm_'+c] = s_shape(project[c])

    open_score = 15*(project['norm_open_issues_count']+ project['norm_open_pr_count'])
    closed_score = 10*(project['norm_closed_issues_count'] + project['norm_closed_pr_count'])

    project =  compute_new_feats(project)
    interest_activity_score = 10*(project['interest'] + project['activity'])

    return open_score + closed_score + interest_activity_score

def binarize_field(value:str):
    """
    Binarize inputs

    Returns: 0 if `value` is NaN or empty string
                1 otherwise
    """
    output = 0
    if (pd.isna(value)):
        output = 0
    elif (value and not value.isspace()):
        output = 1
    return output

def clean(x): 
    """
    Convert string integers (e.g "11,000") into integers (11000)
    """
    if pd.isna(x) : return 0
    if type(x) == str and not x.isspace():
        return int(x.replace(',', ''))
    else:
        return x

def logscale(x):
    """
    Apply a log transform to reduce
    values' scale. 
    Add 1 to avoid infinity.
    """
    return np.log(1.+ x)

def s_shape(x):
    """
    shrink values into [0, 1] and saturate
    later than sigmoid and tanh
    """
    return x/(1. + x)

def compute_new_feats(project):
    """
    Add new features that are ratios of
    open/closed issues and PRs
    P.S: For next iteration, it'll be better to incorporate
    the number of contributors to weight these ratios as
    fewer contributors could imply many open issues.
    """  
    project['interest'] = project.apply(lambda row: row['open_issues_count']/row['closed_issues_count'] \
                                        if row['closed_issues_count'] != 0 else 0, axis=1)
    project['activity'] = project.apply(lambda row: row['open_pr_count']/row['closed_pr_count'] \
                                        if row['closed_pr_count'] != 0 else 0, axis=1)
    new_feats = ['interest', 'activity']
    for c in new_feats:
        project[c] = s_shape(logscale(project[c]))
    return project

if __name__ == '__main__':
    data_csv = pd.read_csv('../data/db_tables/community_table.csv')
    cols = ['open_issues_count','closed_issues_count','open_pr_count','closed_pr_count']
    print(data_csv)
    score = community_score(data_csv)
    data_csv['community_health_score'] = score
    print(data_csv[cols+['community_health_score']])
    data_csv.to_csv('../data/db_tables/community_table.csv', index=False)
