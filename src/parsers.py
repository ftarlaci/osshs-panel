import numpy as np
import re

def parse_python(file):
    """
    Given a code file in the form of a long string, this function
    extracts the imported libraries.

    Parameters
    ----------
    file: str
        The content of a code file.

    Returns
    ----------
    np.array
        An array of unique library names.
    """

    out = []
    regex = "import .*"
    imports = re.findall(regex, file)

    regex = "from .* import .*"
    from_imports = re.findall(regex, file)


    remove = []
    for f in from_imports:
        for i in range(len(imports)):
            if f.split(' ')[-1] in imports[i]:
                remove.append(i)

    remove = np.unique(remove)

    for r in reversed(remove):
        del imports[r]



    for im in imports:
        out.append(im.split(' ')[1].split('.')[0])
    for f in from_imports:
        out.append(f.split(' ')[1].split('.')[0])

    return np.unique(out)