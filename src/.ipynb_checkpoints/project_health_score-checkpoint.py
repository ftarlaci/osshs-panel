import pandas as pd
import numpy as np
from datetime import datetime


class ProjectHealthScore(object):

    """
    This class loads all subscores and
    process them to produce the single health
    score of a project that accounts for 4 aspects:
        - community
        - popularity
        - security
        - licensing
    """

    def __init__(self, security_csv, license_csv, ref_license_csv,
                 popularity_csv, community_csv, projects_csv):
        """
        Load all subscore tables.
        """
        self.community_csv = community_csv
        self.popularity_csv = popularity_csv
        self.security_csv = security_csv
        self.license_csv = license_csv
        self.ref_license_csv = ref_license_csv
        self.projects_csv = projects_csv


    def overall_score(self):
        """
        Compute the aggregated health score of a project based on 4 subscores
        with associated coefficients:
            - security   - 0.4
            - license    - 0.3
            - popularity - 0.2
            - community  - 0.1
        """
        self.compute_subscores()
        joint = pd.merge(self.community, self.popularity, on='project_id', how='outer')
        joint = pd.merge(joint, self.security, on='project_id', how='outer')
        joint = pd.merge(joint, self.license, on='project_id', how='outer')
        joint.fillna(0, inplace=True)
        score = 0.4*joint['scaled_security_scan_score'] + 0.3*joint['permissiveness_score']
        score += 0.2*joint['popularity_health_score'] + 0.1*joint['community_health_score']
        
        self.projects = pd.read_csv(self.projects_csv)
        self.projects['aggregate_health_score'] =  round(score)
        self.projects['datetime_score_last_calculated'] = str(datetime.now()).split('.')[0]
        return self.projects['aggregate_health_score']

    def compute_subscores(self):
        """
        """
        self.community_score()
        self.popularity_score()
        self.license_score()
        self.security_score()


    def community_score(self):
        """
        Compute the community subscore with the following rules:
            - documentation counts for 50 points;
            - contribution guidelines counts for 30 points; 
            - and readme count for 20 points.
        """
        self.community = pd.read_csv(self.community_csv)
        doc = self.community['documentation'].apply(lambda x: self._binarize_field(x))
        contrib = self.community['has_contributing_guidelines'].apply(lambda x: self._binarize_field(x))
        readme = self.community['has_readme'].apply(lambda x: self._binarize_field(x))
        self.community['community_health_score'] = 50*doc + 30*contrib + 20*readme

        return self.community['community_health_score']
    
    def popularity_score(self):
        """
        Computes the popularity subscore by multiplying by the below coefficients.
        """
        self.popularity = pd.read_csv(self.popularity_csv)
        intercept = 19.828634023593512
        coefs = np.array([ 2.01082485e-02,  1.62804534e-04,  8.18015985e-05,
                        -3.60324950e-03,  1.01100484e-03,  9.12123607e-02,
                        -3.32834108e-03])
        columns = ['subscribers_count', 'dependents_count', 'dependent_repos_count',
                'forks_count', 'stargazers_count', 'contributions_count',
                'open_issues_count']
        A = self.popularity[columns]
        self.popularity["popularity_health_score"] = round(A.dot(coefs) + intercept, 3)

        return self.popularity["popularity_health_score"]
        
    def license_score(self):
        """
        """
        self.license = pd.read_csv(self.license_csv)
        self.ref_license = pd.read_csv(self.ref_license_csv)
        self.license = pd.merge(self.license, self.ref_license,
                                on='license', how='outer')
        self.license["permissiveness_score"].fillna(0, inplace=True)
        return self.license["permissiveness_score"]

    def security_score(self):
        """
        Compute the security scan score and 
        Up scale it by 10 to be within [0 - 100]
        as for other scores.
        """
        self.security = pd.read_csv(self.security_csv)
        scores = self.security.security_scan_score.apply(lambda x: str(x).split('/')[0])
        self.security["scaled_security_scan_score"] = (scores.astype(float))*10
        return self.security["scaled_security_scan_score"]


    def save_results(self):
        """
        Saves updated tables with calculated scores
        """
        if self.community is not None:
            self.community.to_csv(self.community_csv, index=None)
        if self.popularity is not None:
            self.popularity.to_csv(self.popularity_csv, index=None)
        # if self.license is not None:
        #     self.license.to_csv(self.license_csv, index=None)
        if self.security is not None:
            self.security.to_csv(self.security_csv, index=None)
        if self.projects is not None:
            self.projects.to_csv(self.projects_csv, index=None)
        

    def _binarize_field(self, value):
        """
        Binarize inputs

        Returns: 0 if `value` is NaN or empty string
                 1 otherwise
        """
        output = 0
        if (pd.isna(value)):
            output = 0
        elif (value and not value.isspace()):
            output = 1
        return output   
    



if __name__ == '__main__':

    path = "../data/db_tables/"
    community_csv = path+'community_table.csv'
    popularity_csv = path+'popularity_table.csv'
    security_csv = path+'security_table.csv'
    license_csv = '../data/licenses_sample.csv'
    ref_license_csv = path+'license_table.csv'
    projects_csv = path+'projects.csv'

    phs = ProjectHealthScore(security_csv, license_csv, ref_license_csv,
                            popularity_csv, community_csv, projects_csv)

    health_scores = phs.overall_score()
    print(health_scores)
    print(health_scores.max())
    print(health_scores.min())
    print(phs.projects.head())
    phs.save_results()


