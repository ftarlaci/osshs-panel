import numpy as np
import pandas as pd




def binarize_field(value:str):
    output = 0
    if (pd.isna(value)):
        output = 0
    elif (value and not value.isspace()):
        output = 1
    return output

def logscale(x):
    """
    Transform number in string value into int value. 
    E.g: '1,000' -> 1000,  NaN -> 0
    """
    if pd.isna(x): return 0
    if type(x) == str and not x.isspace():
        out = int(x.replace(',', ''))
        return np.log(1.+ out)
    else:
        return x

def normalize(df, col):
    max_ = df[col].max()
    min_ = df[col].min()
    df["normalized_"+col] = (df[col] - min_)/(max_ - min_)
    return df

def community_score(project:pd.Series):
    """
    Compute the community subscore with the following rules:
        - documentation counts for 50 points;
        - contribution guidelines counts for 30 points; 
        - and readme count for 20 points.
    """
    
    doc = binarize_field(project['documentation'])
    contrib = binarize_field(project['has_contributing_guidelines'])
    readme = binarize_field(project['has_readme'])
    binary_fields_score = 30*(0.5*doc + 0.3*contrib + 0.2*readme)

    issues_prs = ['open_issues_count', 'closed_issues_count', 'open_pr_count', 'closed_pr_count']
    for c in issues_prs:
        print(project[c])
        project[c] = logscale(project[c])

    print(project[issues_prs]) 
    return binary_fields_score


if __name__ == '__main__':
    data_csv = pd.read_csv('../data/db_tables/community_table.csv')
    data_csv['community_health_score'] = data_csv.apply(lambda row: community_score(row), axis=1)
    first_5_scores = [50, 20, 20, 50, 70]
    for c in 
    # assert list(data_csv['community_health_score'][:5]) == first_5_scores
    print(data_csv.head(5))
    # data_csv.to_csv("../data/db_tables/community_table.csv", index=False)