import pandas as pd
import numpy as np
import subprocess
from datetime import datetime


class ProjectHealthScore(object):

    """
    This class loads all subscores and
    process them to produce the single health
    score of a project that accounts for 4 aspects:
        - community
        - popularity
        - security
        - licensing
    """

    def __init__(self, security_csv, license_csv, popularity_csv,
                 community_csv, projects_csv):
        """
        Load all subscore tables.
        """
        self.community_csv = community_csv
        self.popularity_csv = popularity_csv
        self.security_csv = security_csv
        self.license_csv = license_csv
        self.projects_csv = projects_csv


    def overall_score(self):
        """
        Compute the aggregated health score of a project based on 4 subscores
        with associated coefficients:
            - security   - 0.3
            - license    - 0.3
            - popularity - 0.2
            - community  - 0.2
        """
        self.compute_subscores()
        joint = self._joint_subscores()
        score = 0.3*(joint['scaled_security_scan_score'] + joint['permissiveness_score'])
        score += 0.2*(joint['popularity_health_score'] + joint['community_health_score'])
        
        self.projects = pd.read_csv(self.projects_csv)
        self.projects['aggregate_health_score'] =  round(score)
        joint["aggregate_health_score"] = self.projects['aggregate_health_score']
        self.projects['datetime_score_last_calculated'] = str(datetime.now()).split('.')[0]
        joint['datetime_score_last_calculated'] = str(datetime.now()).split('.')[0]
        return joint

    def compute_subscores(self):
        """
        Compute all 4 components scores.
        """
        self.security_score()
        self.license_score()
        self.popularity_score()
        self.community_score()

    def _joint_subscores(self):
        """
        Create a dataframe containing all
        component scores.
        """
        pid = 'project_id'
        joint = pd.merge(self.community[[pid, 'project_name', 'community_health_score']],
                         self.popularity[[pid, 'popularity_health_score']], 
                         on='project_id', how='outer')
        joint = pd.merge(joint, self.security[[pid, 'scaled_security_scan_score']],
                         on='project_id', how='outer')
        joint = pd.merge(joint, self.license[[pid, 'permissiveness_score']],
                         on='project_id', how='outer')
        joint.fillna(0, inplace=True)
        return joint

    def security_score(self):
        """
        Compute the security scan score and 
        Up scale it by 10 to be within [0 - 100]
        as for other scores.

        This method uses a subprocess to run the external Scorecard 
        app that calculate security scores for each project in the table,
        and thus, it takes a while to run. 
        """
        self.security = pd.read_csv(self.security_csv)
        scores = self.security.security_scan_score.apply(lambda x: str(x).split('/')[0])
        self.security["scaled_security_scan_score"] = (scores.astype(float))*10
        return self.security["scaled_security_scan_score"]

    def license_score(self):
        """
        This method retrieves the score of each license
        and assigns it to the project to be used in the
        overall health score.
        """
        self.license = pd.read_csv(self.license_csv)
        self.license["permissiveness_score"].fillna(0, inplace=True)
        return self.license["permissiveness_score"]


    def popularity_score(self):
        """
        Computes the popularity subscore by multiplying by the below coefficients.
        """
        self.popularity = pd.read_csv(self.popularity_csv)
        coefs = np.array([40,  30.,  12., 9., 6., 3.])
        columns = ['contributions_count', 'subscribers_count', 'stargazers_count', 
                'dependents_count', 'dependent_repos_count', 'forks_count']
        A = self._s_shape(self._logscale(self.popularity[columns]))
        self.popularity["popularity_health_score"] = round(A.dot(coefs), 2)

        return self.popularity["popularity_health_score"]

    
    def community_score(self):
        """
        Compute the community subscore with the following rules:
            - Boolean fields (documentation, contribution guidelines
                and readme) counts for 30 points;
                
            - Non boolean fields (open/closed issues and PRs) 
                counts for 70 points 
        """
        self.community = pd.read_csv(self.community_csv)
        bf_score = self._binary_fields_score()
        nbf_score = self._non_binary_fields_score()
        self.community['community_health_score'] = round(bf_score + nbf_score, 2)
        return self.community['community_health_score']
    
    def _logscale(self, x):
        """
        Apply a log transform to reduce
        values' scale. 
        Add 1 to avoid infinity.
        """
        return np.log(1.+ x)

    def _s_shape(self, x):
        """
        shrink values into [0, 1] and saturate
        later than sigmoid and tanh
        """
        return x/(x+1)

    def _binary_fields_score(self):
        """
        Compute the partial community score (counting for 30 points out of 100)
        based on boolean fields with the following coefficients:
            - documentation          : 0.5
            - conribution guidelines : 0.3
            - readme                 : 0.2
        """
        doc = self.community['documentation'].apply(lambda x: self._binarize_field(x))
        contrib = self.community['has_contributing_guidelines'].apply(lambda x: self._binarize_field(x))
        readme = self.community['has_readme'].apply(lambda x: self._binarize_field(x))
        return 30*(0.5*doc + 0.3*contrib + 0.2*readme)

    def _non_binary_fields_score(self):
        """
        Compute a weighted score based on non binary fields
        (opne/closed issues and PRs)
        """
        issues_prs = ['open_issues_count', 'closed_issues_count',
                      'open_pr_count', 'closed_pr_count']
        df = self.community.copy()
        for c in issues_prs:
            df[c] = df[c].apply(lambda x: self._logscale(self._format_new_fields(x)))
            df['norm_'+c] = self._s_shape(df[c])
                    
        open_score = 15*(df['norm_open_issues_count']+ df['norm_open_pr_count'])
        closed_score = 10*(df['norm_closed_issues_count'] + df['norm_closed_pr_count'])

        df_new_feats = self._community_new_feats()
        interest_activity_score = 10*(df_new_feats['interest'] + df_new_feats['activity'])
        return open_score + closed_score + interest_activity_score


    def save_results(self):
        """
        Saves updated tables with calculated scores
        """
        if self.community is not None:
            self.community.to_csv(self.community_csv, index=False)
        if self.popularity is not None:
            self.popularity.to_csv(self.popularity_csv, index=False)
        # if self.license is not None:
        #     self.license.to_csv(self.license_csv, index=None)
        if self.security is not None:
            self.security.to_csv(self.security_csv, index=False)
        if self.projects is not None:
            self.projects.to_csv(self.projects_csv, index=False)
        

    def _binarize_field(self, value):
        """
        Binarize inputs

        Returns: 0 if `value` is NaN or empty string
                 1 otherwise
        """
        output = 0
        if (pd.isna(value)):
            output = 0
        elif (value and not value.isspace()):
            output = 1
        return output

    def _format_new_fields(self, x):
        """
        Convert string integers (e.g "11,000") into integers (11000)
        """
        if pd.isna(x): return 0
        if type(x) == str and not x.isspace():
            return int(x.replace(',', ''))
        else:
            return x

    def _community_new_feats(self):
        """
        Add new features that are ratios of
        open/closed issues and PRs
        """  
        df = self.community.copy()
        df['interest'] = df.apply(lambda row: row['open_issues_count']/row['closed_issues_count'] \
                                            if row['closed_issues_count'] != 0 else 0, axis=1)
        df['activity'] = df.apply(lambda row: row['open_pr_count']/row['closed_pr_count'] \
                                            if row['closed_pr_count'] != 0 else 0, axis=1)
        new_feats = ['interest', 'activity']
        for c in new_feats:
            df[c] = self._s_shape(self._logscale(1. + df[c]))
        return df
        
        




if __name__ == '__main__':

    path = "../data/db_tables/"
    community_csv = path+'community_table.csv'
    popularity_csv = path+'popularity_table.csv'
    security_csv = path+'security_table.csv'
    license_csv = '../data/license_score_with_projects.csv'
    projects_csv = path+'projects.csv'
    phs = ProjectHealthScore(security_csv, license_csv, popularity_csv,
                             community_csv, projects_csv)

    
    cols = ['project_name', 
            'scaled_security_scan_score', 
            'permissiveness_score', 
            'popularity_health_score', 
            'community_health_score',
            'aggregate_health_score'
            ]
    # print stats about results
    health_scores = phs.overall_score()
    max_score = phs.projects['aggregate_health_score'].max()
    min_score = phs.projects['aggregate_health_score'].min()
    print(health_scores[health_scores.aggregate_health_score==max_score][cols].T)
    print(health_scores[health_scores.aggregate_health_score==min_score][cols].T)
    print(health_scores[health_scores.project_name=='pytorch/pytorch'][cols].T)

    # update projects.csv
    print(phs.projects.head().T)
    phs.projects.to_csv(path+'projects.csv', index=False)