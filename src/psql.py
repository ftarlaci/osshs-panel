from config import config
from psycopg2 import connect, extensions, sql


# declare a new PostgreSQL connection object
conn = connect(
dbname = "osshs",
user = "openteams",
host = "localhost",
password = ""
)

# object type: psycopg2.extensions.connection
print ("\ntype(conn):", type(conn))


# def create_tables():
#     """ create tables in the PostgreSQL database"""
#     commands = (
#         """
#         CREATE TABLE projects (
#             project_id INTEGER PRIMARY KEY,
#             project_name VARCHAR(255) NOT NULL
#         )
#         """,
#         """
#         """)
#     conn = psycopg2.connect(dbname='osshs',
#       user=user_name, host='',
#       password=password)

#     try:
#         # read the connection parameters
#         params = config()
#         # connect to the PostgreSQL server
#         conn = psycopg2.connect(**params)
#         cur = conn.cursor()
#         # create table one by one
#         for command in commands:
#             cur.execute(command)
#         # close communication with the PostgreSQL database server
#         cur.close()
#         # commit the changes
#         conn.commit()
#     except (Exception, psycopg2.DatabaseError) as error:
#         print(error)
#     finally:
#         if conn is not None:
#             conn.close()


# if __name__ == '__main__':
#     create_tables()