import numpy as np
import base64
import re

from github import Github
from .parsers import parse_python

file_extensions = {
    'python': ['.py'],
}


parsers = {
    'python': parse_python,
}

class Environment:
    """
    A class for extracting the dependencies of a given gh repository.

    Parameters
    ----------
    repo_name: str
        The content of a code file.

    language: str
        The main language of the repository.

    token: str
        The gh token to use.
    """
    def __init__(self, repo_name, language, token=None):
        self.repo_name = repo_name
        self.gh = Github(token).get_repo(self.repo_name)
        self.language = language

    def get_repo_dependencies(self):
        """
        A summarizing function that returns the dependencies. It gets the file
        contents with `get_github_contents` and parses and aggregates the
        dependencies with `concat_imports`.
        """
        # start with main directory using '' and give the function call
        # an empty list to start the aggregation.
        contents = self.get_github_contents('', [])
        return self.concat_imports(contents)

    def get_github_contents(self, directory, file_list):
        """
        A recursive function to collect the file contents for files of a specified type.
        The function starts by finding all of the files in a given directory. It then
        loops through all of the contents and checks whether it has the right file extension
        or if it is a directory, in which case we make a recursive call with that directory.
        If the file extension matches, we decode the content to a python str and append it
        to our ongoing list of files.

        Parameters
        ----------
        directory: str
            The name of the directory to parse.

        file_list: list[str]
            The ongoing list of contents

        Returns
        ----------
        list:
            The updated list of contents
        """
        contents = self.gh.get_contents(directory)
        for content in contents:
            if content.type == 'dir':
                file_list = self.get_github_contents(content.path, file_list)
            elif any(ext in content.path for ext in file_extensions[self.language]):
                try:
                    decoded = base64.b64decode(content.content).decode("utf-8")
                    file_list.append(decoded)
                except UnicodeDecodeError:
                    # just in case something else gets picked up
                    continue
        return file_list

    def concat_imports(self, files):
        """
        A function to parse each file for imports and return an array of
        unique imports.
        """
        imports = []
        for f in files:
            imports.append(parsers[self.language](f))
        return np.unique(np.concatenate(imports))