import numpy as np
import pandas as pd


def popularity_score(df):
    """
    Computes the popularity subscore by multiplying by the below coefficients.
    """
    coefs = np.array([40,  30.,  12., 9., 6., 3.])
    columns = ['contributions_count', 'subscribers_count', 'dependent_repos_count',
                'stargazers_count', 'dependents_count', 'forks_count']
    A = s_shape(logscale(df[columns]))
    return round(A.dot(coefs), 2)
    
def s_shape(x):
    """
    shrink values into [0, 1] and saturate
    later than sigmoid and tanh
    """
    return x/(x+1.)

def logscale(x):
    """
    Apply a log transform to reduce
    values' scale. 
    Add 1 to avoid infinity.
    """
    return np.log(1.+x)



if __name__ == '__main__':
    df_pop = pd.read_csv('../data/db_tables/popularity_table.csv')
    print(df_pop.tail(10))
    df_clean = df_pop.fillna(0)
    score = popularity_score(df_clean)
    df_pop['popularity_health_score'] = score
    print(df_pop)
    print(score.max(), score.min(), score.median(), score.mean())

    df_pop.to_csv("../data/db_tables/popularity_table.csv", index=False)
