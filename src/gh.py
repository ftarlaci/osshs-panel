from github import Github
import numpy as np
import base64

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.errorhandler import NoSuchElementException



def browser_prep(headless=True):
    """
    Prepares some browser settings for selenium.
    """
    useOptions = Options()
    useOptions.add_experimental_option('excludeSwitches', ['enable-logging'])
    useOptions.add_argument('window-size=1920x1080')
    useOptions.add_argument("--no-sandbox")
    useOptions.add_argument("--disable-dev-shm-usage")
    useOptions.add_argument("start-maximised")
    if headless:
        useOptions.add_argument("--headless")
    browser = webdriver.Chrome(ChromeDriverManager().install(), options=useOptions)
    browser.maximize_window()
    return browser

class GithubProject:

    def __init__(self, name):
        self.project_name = name
        self.github = Github().get_repo(name)

        self.stars_count = self.github.stargazers_count
        self.forks_count = self.github.forks_count

        self.get_license()
        self.get_release_rate()
        self.contributor_company_diversity(100)
        self.locate_readthedocs()



    def get_license(self):
        self.license = base64.b64decode(self.github.get_license().content)

    def get_release_rate(self):
        releases = self.github.get_releases()
        dates = [r.published_at for r in releases]
        deltas = [(dates[i] - dates[i+1]).days for i in range(len(dates) - 1)]
        self.release_rate = np.mean(deltas)

    def contributor_company_diversity(self, n_users):
        users = self.github.get_contributors()
        companies = {}
        for user in users[:n_users]:
            company = user.company
            if company in companies:
                companies[company] += 1
            elif company is None:
                continue
            elif ', ' in company:
                cps = company.split(', ')
                for c in cps:
                    if c in companies:
                        companies[c] += 1
                    else:
                        companies[c] = 1
            else:
                companies[company] = 1
        self.companies = companies

    def locate_readthedocs(self):
        driver = browser_prep()
        driver.get(f'https://readthedocs.org/projects/{self.project_name}/')
        try:
            link = driver.find_element_by_link_text('stable')
            url = link.get_attribute('href')
        except NoSuchElementException:
            try:
                link = driver.find_element_by_link_text('latest')
                url = link.get_attribute('href')
                except NoSuchElementException:
                    self.doc_sources['readthedocs'] = None
                    return

        self.readthedocs = url
