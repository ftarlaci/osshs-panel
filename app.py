import numpy as np
import pandas as pd
import panel as pn
import holoviews as hv
hv.extension('bokeh')
pn.extension()

from scipy.stats import zscore

def convert_sec_score(score):
    """
    The security scores are stored as strings of the
    form score/10 (4.5/10). If a project has a score
    we'll extract it. Otherwise, we'll return nan.
    """
    try:
        return float(score.split('/')[0])
    except: return np.nan

def find_range(val):
    """
    The function helps to determine where a project
    sits among others. It is used to dispatch the
    the markdown used to describe a project.
    """
    if val <= -0.5:
        return 'low'
    elif -0.5 < val < 0.5:
        return 'mid'
    return 'high'

def lic_score(df, p_id):
    """
    This returns the permissiveness and license
    for a project specified by `p_id`. If the
    project has no license it returns nan for
    both.
    """
    out = df[df.project_id==p_id]
    if out.size==0:
        return np.nan, np.nan
    return out.permissiveness_score.tolist()[0], out.license.tolist()[0]

def shorten_names(name):
    return name.split('/')[-1].capitalize()

def str_to_int(val):
    try:
        return int("".join(val.split(',')))
    except AttributeError:
        return val
class App:
    """
    The main object for organizing the panel app.
    """
    def __init__(self,
        community_data = 'data/db_tables/community_table.csv',
        license_data = 'data/license_score_with_projects.csv',
        popularity_data = 'data/db_tables/popularity_table.csv',
        security_data = 'data/db_tables/security_table.csv',
        total_score = 'data/db_tables/projects.csv',
        ):

        # load data
        self.community_data = pd.read_csv(community_data)
        self.license_data = pd.read_csv(license_data)
        self.popularity_data = pd.read_csv(popularity_data)
        self.security_data = pd.read_csv(security_data)
        self.total_score_data = pd.read_csv(total_score)

        # some useful resources
        self.id_mapping = {name: project_id for name, project_id in zip(
            self.popularity_data.project_name, self.popularity_data.project_id)}
        self.name_mapping = {partial_name: full_name for partial_name, full_name  in zip(
            self.popularity_data.project_name.apply(shorten_names), self.popularity_data.project_name
        )}
        project_id = self.popularity_data.project_id
        security_score = self.security_data.security_scan_score.apply(convert_sec_score)


        # compute z scores
        self.zscore_popularity = zscore(self.popularity_data[['subscribers_count', 'dependents_count',
           'dependent_repos_count', 'forks_count', 'stargazers_count',
           'contributions_count', 'popularity_health_score']])

        self.zscore_popularity['project_id'] = self.popularity_data.project_id
        self.zscore_community = pd.concat([self.community_data.project_id,
            zscore(self.community_data.community_health_score, nan_policy='omit'),
            zscore(self.community_data.open_issues_count.apply(str_to_int), nan_policy='omit'),
            zscore(self.community_data.closed_issues_count.apply(str_to_int), nan_policy='omit'),
            zscore(self.community_data.open_pr_count.apply(str_to_int), nan_policy='omit'),
            zscore(self.community_data.closed_pr_count.apply(str_to_int), nan_policy='omit')], axis=1)
        self.zscore_license = pd.concat([self.license_data.project_id,
            self.license_data.license,
            zscore(self.license_data.permissiveness_score)], axis=1)
        self.zscore_security = pd.concat([project_id,
            zscore(security_score, nan_policy='omit')], axis=1)
        self.zscore_total = pd.concat([self.total_score_data.project_id,
            zscore(self.total_score_data.aggregate_health_score)], axis=1)

        # setup widgets

        # selector
        self.project_select = pn.widgets.Select(
            options=self.popularity_data.project_name.apply(shorten_names).tolist(), width=200)
        self.repo_input = pn.widgets.TextInput(placeholder='Enter a project name (i.e. wp-cli/wp-cli)')
        self.button = pn.widgets.Button(name='Run',
                                        button_type='primary',
                                        width=200)
        self.warning_markdown = pn.pane.Markdown('',width=200)
        self.input_row = pn.Row(self.project_select, self.repo_input, self.button, self.warning_markdown)
        self.button.on_click(self.click_update) # updates on click or selecter change

        # total score
        self.score_markdown = pn.pane.Markdown(width=500)
        self.main_plot = pn.pane.HoloViews(object=None, width=700, height=225)
        self.score_row = pn.Row(self.score_markdown, self.main_plot, height=225)

        # popularity
        self.popularity_markdown = pn.pane.Markdown(width=500)
        self.popularity_plot = pn.pane.HoloViews(object=None, width=700, height=325)
        self.popularity_row = pn.Row(self.popularity_markdown, self.popularity_plot, height=350)

        # community
        self.community_markdown = pn.pane.Markdown(width=500)
        self.community_plot1 = pn.pane.HoloViews(object=None, width=300, height=325)
        self.community_plot2 = pn.pane.HoloViews(object=None, width=350, height=325)
        self.community_row = pn.Row(self.community_markdown, self.community_plot1,
            self.community_plot2, height=350)

        # license
        self.license_markdown = pn.pane.Markdown(width=500, height=125)

        # security
        self.security_markdown = pn.pane.Markdown(width=500, height=125)

        # whole layout
        self.layout = pn.Column(self.input_row,
                                    self.score_row,
                                    self.popularity_row,
                                    self.community_row,
                                    self.license_markdown,
                                    self.security_markdown)

        # define responses
        self.score_responses = {'low': "has a low score. Its score is below the mean by at least half \
                        of a standard deviation",
                          'mid': "has a medium score. Its score falls within half of a standard deviation \
                        of the mean.",
                          'high': "has a high score. Its score is greater than the mean by at least \
                        half of a standard deviation."}
        self.main_procedure = "After calculating the subscore for each individual component, we generated an \
            aggregate score by performing a weighted sum."


        self.project_select.param.watch(self.select_update, "value")
        self.select_update('') # app starts out empty. Call update initially to load contents
        #self.layout.show()
        #self.layout.servable()

    def click_update(self, event):
        """
        Checks if the text input is valid. If so it
        proceeds to set the `current_name` and then
        calls update.
        """
        if self.name_mapping[self.repo_input.value] not in self.id_mapping.keys():
            self.warning_markdown.object = 'No data found for that project.'
            return
        self.project_select.value = self.repo_input.value
        self.current_name = self.name_mapping[self.repo_input.value]
        self.update()

    def select_update(self, event):
        """
        Sets `current_name` and then calls update.
        """
        self.current_name = self.name_mapping[self.project_select.value]
        self.update()

    def get_data(self):
        """
        Collects all of the data for a specific project.
        Includes preparing data for the barplots which are
        specified in the form:
        [('label1', scaler_value1), ('label2',scaler_value2),...]
        """
        current_id = self.id_mapping[self.current_name]
        self.current_zscores = {
            'popularity': self.zscore_popularity[
                self.zscore_popularity.project_id==current_id].popularity_health_score.tolist()[0],
            'community': self.zscore_community[
                self.zscore_community.project_id==current_id].community_health_score.tolist()[0],
            'security': self.zscore_security[
                self.zscore_security.project_id==current_id].security_scan_score.tolist()[0],
            'license': lic_score(self.zscore_license, current_id)[0],
        }

        self.current_subscores = {
            'popularity': self.popularity_data[
                self.popularity_data.project_id==current_id].popularity_health_score.tolist()[0],
            'community': self.community_data[
                self.community_data.project_id==current_id].community_health_score.tolist()[0],
            'security': self.security_data[
                self.security_data.project_id==current_id].security_scan_score.tolist()[0],
            'license': lic_score(self.license_data, current_id)[0]
        }

        self.current_total_score = self.total_score_data[
            self.total_score_data.project_id==current_id].aggregate_health_score.tolist()[0]
        self.current_total_zscore = self.zscore_total[
            self.zscore_total.project_id==current_id].aggregate_health_score.tolist()[0]

        self.current_license = lic_score(self.license_data, current_id)[1]

        current_popularity_zscores = self.zscore_popularity[
                self.zscore_popularity.project_id==current_id]
        columns = ['subscribers_count', 'dependents_count',
           'dependent_repos_count', 'forks_count', 'stargazers_count',
           'contributions_count']
        names = ['Followers', 'Dep. pros', 'Dep. Repos', 'Forks', 'Stars',
                 'Contributors', 'Open Issues']
        self.popularity_plot_data = [(n, current_popularity_zscores[c].tolist()[0]) for c,n in zip(columns, names)]

        components = ['popularity', 'community', 'security', 'license']
        self.main_plot_data = [(c, self.current_zscores[c]) for c in components]

        current_community_data = ~self.community_data.isna()[
                self.community_data.project_id==current_id]

        columns = ['documentation', 'has_contributing_guidelines',
           'has_readme']
        names = ['documentation', 'contributing', 'readme']
        self.community_plot1_data = [(n, current_community_data[c].tolist()[0]) for c,n in zip(columns, names)]

        current_community_zscores = self.zscore_community[
                self.zscore_community.project_id==current_id]

        columns = ['open_issues_count', 'closed_issues_count',
           'open_pr_count', 'closed_pr_count']
        names = ['Open Issues', 'Closed Issues', 'Open PRs', 'Closed PRs']
        self.community_plot2_data = [(n, current_community_zscores[c].tolist()[0]) for c,n in zip(columns, names)]



    def update(self):
        """
        Fills all of the panel objects with data. This gets
        called regardless of whether the change was induced
        by a selector change of a text input.
        """

        # update data
        self.get_data()

        # update markdown text

        # remove warning if it's present
        self.warning_markdown.object = ''

        self.score_markdown.object = f"""
## OSSHS: {self.current_total_score} <br>
**{self.current_name}** {self.score_responses[find_range(self.current_total_zscore)]}<br><br>

{self.main_procedure}"""
        self.main_plot.object = hv.Bars(self.main_plot_data, 'Health Score Components', 'z scores')

        # popularity
        self.popularity_markdown.object = f"""
## Popularity Score: {str(np.round(self.current_subscores['popularity'], decimals=2))}/100

For the popularity subscore we used a linear combination of the following seven variables:\n
- Followers Count\n
- Dependent Project\n
- Count\n
- Dependent Repository Count
- Forks Count\n
- Stars Count\n
- Contributors Count"""
        self.popularity_plot.object = hv.Bars(self.popularity_plot_data, 'Popularity Variables', 'z scores')

        # community
        self.community_markdown.object = f"""
## Community Score: {self.current_subscores['community']}/100

The community subscore consists of a linear combination of the
following variables:\n
- Has a documentation page\n
- has contributing guidelines\n
- has a readme\n
- open issues count\n
- closed issues count\n
- open pull request count\n
- closed pull request count\n
"""
        self.community_plot1.object = hv.Bars(self.community_plot1_data, 'Community Variables', 'True/False')
        self.community_plot2.object = hv.Bars(self.community_plot2_data, 'Other Community Variables', 'z scores')

        # license
        self.license_markdown.object = f"""
## License Score: {self.current_subscores['license']}/100\n

The license subscore is based on how permissive the license is considered to be and whether it is a copyleft license or not. """
        if self.current_license is not np.nan:
            self.license_markdown.object += f"This project uses {self.current_license}."
        else:
            self.license_markdown.object += "This project has no license listed in their repository."
        # security
        self.security_markdown.object = f"""
## Security Score: {convert_sec_score(self.current_subscores['security']) * 10}/100

For security score, we use the application named Scorecards
originally developed by Google. Scorecards is released under the OpenSSF.
Scorecards auto-generates a “security score” for open source
projects based on the components below:\n

- **Token-Permissions:** This check determines whether the project's automated workflows tokens are set to read-only by default.\n
- **Vulnerabilities:** This check determines whether the project has open, unfixed vulnerabilities using the OSV (Open Source Vulnerabilities) service. An open vulnerability is readily exploited by attackers and should be fixed as soon as possible.\n 
- **Fuzzing:** This check tries to determine if the project uses fuzzing by checking if the repository name is included in the OSS-Fuzz project list.\n 
- **Security-Policy:**  This check tries to determine if the project has published a security policy. It works by looking for a file named SECURITY.md\n 
- **Dependency-Update-Tool:** This check tries to determine if the project uses a dependency update tool, specifically dependabot or renovatebot. Out-of-date dependencies make a project vulnerable to known flaws and prone to attacks.\n 
- **Pinned-Dependencies:** This check tries to determine if the project pins its dependencies. A "pinned dependency" is a dependency that is explicitly set to a specific hash instead of allowing a mutable version or range of versions.\n 
- **Code-Review:** This check determines whether the project requires code review before pull requests (merge requests) are merged.\n 
- **Branch-Protection:** This check determines whether a project's default and release branches are protected with GitHub's branch protection settings.\n 
- **CI-Tests:** This check tries to determine if the project runs tests before pull requests are merged.\n 
- **Signed-Releases:** This check tries to determine if the project cryptographically signs release artifacts. Signed releases attest to the provenance of the artifact.\n 
- **CII-Best-Practices:** This check determines whether the project has earned a CII Best Practices Badge, which indicates that the project uses a set of security-focused best development practices for open source software.\n  
- **Maintained:** This check determines whether the project is actively maintained. If the project is archived, it receives the lowest score. If there is at least one commit per week during the previous 90 days, the project receives the highest score.\n 
- **SAST:** This check tries to determine if the project uses Static Application Security Testing (SAST), also known as static code analysis.\n 
- **Contributors:** This check tries to determine if the project has recent contributors from multiple organizations (e.g., companies).\n 
- **Packaging:** This check tries to determine if the project is published as a package.\n 
- **Binary-Artifacts:** This check determines whether the project has generated executable (binary) artifacts in the source repository.\n 
"""

if __name__.startswith("bokeh"):
    App().whole_layout.servable()
